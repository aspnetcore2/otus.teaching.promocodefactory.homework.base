﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpPost("AddEmployee")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> AddEmployeeAsync([FromBody]Employee empl)
        {
            if (empl == null)
                return BadRequest();

            if (empl.Id == Guid.Empty)
                empl.Id = Guid.NewGuid();

            await _employeeRepository.AddAsync(empl);

            return CreatedAtAction("GetEmployeeById", new { id = empl.Id }, empl);
        }

        [HttpDelete("DeleteEmployee")]
        public async Task<IActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            await _employeeRepository.DeleteByIdAsync(id);

            return Ok();
        }

        [HttpPut("UpdateEmployee")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> UpdateEmployee([FromBody]Employee empl)
        {
            if (empl == null)
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(empl.Id);

            if (employee == null)
                return NotFound();

            await _employeeRepository.UpdateAsync(empl);

            return CreatedAtAction("GetEmployeeById", new { id = empl.Id }, empl);
        }
    }
}